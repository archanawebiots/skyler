/* _____________________________________

 Pre loader
 _____________________________________ */
$(window).on('load', function() {
    $('.loader').fadeOut('slow');
    $('.loader').remove('slow');
});

// Home slider
$('#home-slider').owlCarousel({
    loop:true,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:1,
    responsiveClass:true
})

// teatimonial slider
$('#teatimonial-slider').owlCarousel({
    loop:true,
    margin: 30,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:1,
    responsiveClass:true
})

// collection-slider
$('#collection-slider').owlCarousel({
    loop:true,
    margin: 30,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:5,
    responsiveClass:true
})
$('#collection-slider-2').owlCarousel({
    loop:true,
    margin: 30,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:5,
    responsiveClass:true
})
$('#collection-slider-3').owlCarousel({
    loop:true,
    margin: 30,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:5,
    responsiveClass:true
})
$('#special-product-slider').owlCarousel({
    loop:true,
    margin: 30,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:5,
    responsiveClass:true
})

$('#partner-slider').owlCarousel({
    loop:true,
    margin: 110,
    nav: true,
    navClass: ['owl-prev', 'owl-next'],
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause: true,
    items:6,
    responsiveClass:true
})

$(window).on('load',function(){
    $('#themeModal').modal('show');
});
